#!/bin/bash
CI_REGISTRY=${CI_REGISTRY:-registry.gitlab.com}
CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/nubodev/carla}
IMAGE_NAME=$CI_REGISTRY_IMAGE:latest

# build the Docker image (this will use the Dockerfile in the root of the repo)
docker build -t $IMAGE_NAME -f Dockerfile.carla .
# authenticate with the Docker Hub registry
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" $CI_REGISTRY
# push the new Docker image to the Docker registry
docker push $IMAGE_NAME