#!/bin/bash

# Build CARLA-Autoware
mkdir -p carla_autoware_ws/src
cd carla_autoware_ws/src
git clone https://github.com/Nubonetics-Solutions/carla-autoware.git
cd ..
sudo bash -c "source /opt/ros/melodic/setup.bash;
              source /opt/AutowareAI/setup.bash;
              echo $ROS_DISTRO;
              AUTOWARE_COMPILE_WITH_CUDA=1;
              colcon build --merge-install --install-base /opt/carla-autoware --cmake-args -DCMAKE_BUILD_TYPE=Debug"
sudo mkdir -p /opt/carla-autoware/src
sudo rsync -r --links --copy-unsafe-links  ./src /opt/carla-autoware/
cd ..

# Package CARLA and dependencies
sudo chmod -R og-w /opt/carla
tar cfz carla.tar.gz /opt/carla
sudo chmod -R og-w /opt/carla-ros-bridge
tar cfz carla-ros-bridge.tar.gz /opt/carla-ros-bridge
sudo chmod -R og-w /opt/carla-autoware
tar cfz carla-autoware.tar.gz /opt/carla-autoware